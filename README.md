# Migrate Process Vardump

Often used for debugging, this module takes any data given to it and dumps it to the terminal output and then passes it on. This is very useful when you’re writing migrations!

## Usage

Use like any other process plugin:

```yaml
  field_my_field:
    -
      plugin: some_plugin
      source: my_source
    -
      plugin: vardump
    -
      plugin: some_other_plugin
```

If it gets confusing, you can specify a `header` to display before the vardump:

```yaml
  field_my_field:
    -
      plugin: vardump
      source: my_source
      header: 'Before some_plugin'
    -
      plugin: some_plugin
    -
      plugin: vardump
      header: 'After some_plugin'
    -
      plugin: some_other_plugin
```

The vardump process plugin acts as a passthrough. It does not modify the field value in the pipeline.